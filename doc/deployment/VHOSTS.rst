Apache + mod-wsgi configuration
===============================

An example Apache2 vhost configuration follows::

    WSGIDaemonProcess utter_api-<target> threads=5 maximum-requests=1000 user=<user> group=staff
    WSGIRestrictStdout Off

    <VirtualHost *:80>
        ServerName my.domain.name

        ErrorLog "/srv/sites/utter_api/log/apache2/error.log"
        CustomLog "/srv/sites/utter_api/log/apache2/access.log" common

        WSGIProcessGroup utter_api-<target>

        Alias /media "/srv/sites/utter_api/media/"
        Alias /static "/srv/sites/utter_api/static/"

        WSGIScriptAlias / "/srv/sites/utter_api/src/utter_api/wsgi/wsgi_<target>.py"
    </VirtualHost>


Nginx + uwsgi + supervisor configuration
========================================

Supervisor/uwsgi:
-----------------

.. code::

    [program:uwsgi-utter_api-<target>]
    user = <user>
    command = /srv/sites/utter_api/env/bin/uwsgi --socket 127.0.0.1:8001 --wsgi-file /srv/sites/utter_api/src/utter_api/wsgi/wsgi_<target>.py
    home = /srv/sites/utter_api/env
    master = true
    processes = 8
    harakiri = 600
    autostart = true
    autorestart = true
    stderr_logfile = /srv/sites/utter_api/log/uwsgi_err.log
    stdout_logfile = /srv/sites/utter_api/log/uwsgi_out.log
    stopsignal = QUIT

Nginx
-----

.. code::

    upstream django_utter_api_<target> {
      ip_hash;
      server 127.0.0.1:8001;
    }

    server {
      listen :80;
      server_name  my.domain.name;

      access_log /srv/sites/utter_api/log/nginx-access.log;
      error_log /srv/sites/utter_api/log/nginx-error.log;

      location /500.html {
        root /srv/sites/utter_api/src/utter_api/templates/;
      }
      error_page 500 502 503 504 /500.html;

      location /static/ {
        alias /srv/sites/utter_api/static/;
        expires 30d;
      }

      location /media/ {
        alias /srv/sites/utter_api/media/;
        expires 30d;
      }

      location / {
        uwsgi_pass django_utter_api_<target>;
      }
    }
