{
    "swagger": "2.0",
    "info": {
        "title": "Utter API",
        "description": "API to retrieve utters for a municipality",
        "version": "v1"
    },
    "host": "utter-api.com",
    "schemes": [
        "https"
    ],
    "basePath": "/api",
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "securityDefinitions": {
        "Basic": {
            "type": "basic"
        }
    },
    "security": [
        {
            "Basic": []
        }
    ],
    "paths": {
        "/utters/{utter_name}/": {
            "get": {
                "operationId": "utters_read",
                "description": "Haal een utter op",
                "parameters": [
                    {
                        "name": "channel",
                        "in": "query",
                        "description": "Naam van het kanaal",
                        "type": "string"
                    },
                    {
                        "name": "utter_name",
                        "in": "path",
                        "description": "Naam van de utter",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/UtterResponse"
                        }
                    }
                },
                "tags": [
                    "utters"
                ]
            },
            "parameters": [
                {
                    "name": "utter_name",
                    "in": "path",
                    "required": true,
                    "type": "string"
                }
            ]
        }
    },
    "definitions": {
        "UtterResponse": {
            "required": [
                "text",
                "buttons",
                "elements",
                "attachments"
            ],
            "type": "object",
            "properties": {
                "text": {
                    "title": "Text",
                    "description": "The text to return in the response",
                    "type": "string",
                    "minLength": 1
                },
                "buttons": {
                    "description": "Buttons that will be shown in the response",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "image": {
                    "title": "Image",
                    "description": "Optional image to be returned in the response",
                    "type": "string",
                    "minLength": 1
                },
                "elements": {
                    "description": "CURRENTLY NOT USED",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "attachments": {
                    "description": "CURRENTLY NOT USED",
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            }
        }
    }
}
