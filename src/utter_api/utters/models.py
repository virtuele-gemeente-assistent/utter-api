import re
import logging

from django.db import models
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger(__name__)


class Channel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class UtterResponse(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    utter = models.CharField(max_length=2550, help_text="Naam van de utter")
    response = models.TextField(help_text="Antwoord van de chatbot")
    image = models.URLField(
        blank=True,
        null=True,
        help_text="Verwijzing naar een afbeelding wat de chatbot doorstuurt, optioneel",
    )
    channel = models.ForeignKey(
        Channel,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="In te vullen indien het antwoord kanaal-specifiek is",
    )
    custom_html_answer = models.BooleanField(
        default=False,
        help_text="Of het antwoord in HTML is opgemaakt (custom utter template)",
    )
    type = models.CharField(
        max_length=50,
        blank=True,
        choices=[
            ("", "default"),
            ("ask_affirmation", "ask_affirmation (add top 2 intents as buttons)"),
        ],
    )

    class Meta:
        verbose_name = "Antwoord"
        verbose_name_plural = "Antwoorden"

    def __str__(self):
        return "{}: {}".format(self.utter, self.response)

    def get_button_data(self):
        buttons = list(self.buttons.values("title", "payload"))
        return buttons

    # def get_intent_label(self, intent, gemeente, product):
    #     """
    #     Based on an intent from Rasa ("product_paspoort_aanvragen"), determine the correct
    #     Suggestion and return the textual representation of that label.

    #     If the intent is not available, create a new Suggestion and use the intent-name as
    #     the default label
    #     """
    #     existing_labels = Suggestion.objects.filter(intent=intent)

    #     # Gemeente+product-specific check
    #     gem_prod_label = existing_labels.filter(gemeente=gemeente, product=product)
    #     if gem_prod_label:
    #         return gem_prod_label[0].label

    #     # Gemeente-specific check
    #     gem_label = existing_labels.filter(gemeente=gemeente)
    #     if gem_label:
    #         return gem_label[0].label

    #     # Product-specific label check
    #     prod_label = existing_labels.filter(product=product)
    #     if prod_label:
    #         return prod_label[0].label

    #     # No specific label found
    #     intent_label, created = Suggestion.objects.get_or_create(
    #         intent=intent, defaults={"label": intent}
    #     )
    #     return intent_label.label

    # def get_affirmation_buttons(self, intent_ranking, gemeente, product):
    #     """
    #     Based on the intent_ranking from Rasa, return one or two possible intent-buttons with a sufficient confidence level.
    #     """
    #     if len(intent_ranking) > 1:
    #         diff_intent_confidence = intent_ranking[0].get(
    #             "confidence"
    #         ) - intent_ranking[1].get("confidence")
    #         if diff_intent_confidence < 0.2:
    #             intent_ranking = intent_ranking[:2]
    #         else:
    #             intent_ranking = intent_ranking[:1]

    #     first_intent_names = [
    #         intent.get("name", "")
    #         for intent in intent_ranking
    #         if intent.get("name", "") != "out_of_scope"
    #     ]

    #     # entities = tracker.latest_message.get("entities", [])
    #     # entities = {e["entity"]: e["value"] for e in entities}
    #     # logger.info(entities)
    #     entities_json = {}  # json.dumps(entities)

    #     buttons = []
    #     buttons_test = buttons
    #     for intent in first_intent_names:
    #         logger.info(intent)
    #         buttons.append(
    #             {
    #                 "title": "{}".format(
    #                     self.get_intent_label(intent, gemeente, product)
    #                 ),
    #                 "payload": "/{}".format(intent),  # entities_json?
    #             }
    #         )

    #     buttons.append({"title": "Iets anders", "payload": "/negative"})
    #     return buttons

    def fill_in_slots(self, text, slots):
        """
        Based on a textual response, fill in any slots (or arguments) we have received from Rasa.
        Ignore any empty slots.

        Eg.:

        Wil je je {product} aanvragen?

        with slots: {product: 'verhuizing'} will be converted to:

        Wil je je verhuizing aanvragen?
        """
        if not slots:
            return text
        for key, value in slots.items():
            if key and value:
                if isinstance(value, list):
                    value = value[0]
                if not isinstance(value, str):
                    continue
                text = text.replace("{%s}" % key, value)
        return text

    def data(
        self,
        # gemeente=None,
        # product=None,
        slots=None,
        arguments=None,
        # intent_ranking=None,
    ):
        # if self.custom_html_answer:
        #     return {
        #         "custom": {
        #             "html": self.fill_in_slots(
        #                 self.fill_in_slots(self.response, slots), arguments
        #             )
        #         }
        #     }

        # buttons = self.get_button_data()
        # if self.type == "ask_affirmation":
        #     buttons = self.get_affirmation_buttons(intent_ranking, gemeente, product)

        text = self.format_hyperlinks(self.fill_in_slots(
            self.fill_in_slots(self.response, slots), arguments
        ))

        response = {
            "text": text,
            # "buttons": buttons,
            "buttons": [],
            "image": self.image,
            "elements": [],
            "attachments": [],
        }
        return response

    @staticmethod
    def format_hyperlinks(response):
        formatted_response = response
        def repl(matchobj):
            path = matchobj.group(0)[1:-1]
            name, attribute = path.split('.')
            try:
                return getattr(Hyperlink.objects.get(name=name), attribute)
            except models.ObjectDoesNotExist as e:
                logger.exception(f"Exception occurred while trying to format hyperlinks: {e}")
                return matchobj.group(0)

        return re.sub(r'<[A-z\.\_]*>', repl, response)

class Hyperlink(models.Model):
    name = models.CharField(max_length=255, help_text=_(
        "De naam van deze hyperlink, zoals hij in utters opgenomen is."
    ))
    label = models.CharField(max_length=255, help_text=_(
        "De tekst klikbare tekst die weergegeven wordt aan de gebruiker."
    ))
    url = models.URLField(max_length=2000, help_text=_(
        "De URL die naar de gebruiker gestuurd wordt."
    ))

    class Meta:
        verbose_name = "Hyperlink"
        verbose_name_plural = "Hyperlinks"

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.label, self.url)
