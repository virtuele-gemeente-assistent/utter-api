import logging

from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.viewsets import ModelViewSet

from ..models import UtterResponse
from .serializers import UtterResponseSerializer

logger = logging.getLogger(__name__)

UTTER_NAME_PATH_PARAM = openapi.Parameter(
    "utter_name",
    openapi.IN_PATH,
    description="Naam van de utter",
    type=openapi.TYPE_STRING,
)

CHANNEL_QUERY_PARAM = openapi.Parameter(
    "channel",
    openapi.IN_QUERY,
    description="Naam van het kanaal",
    type=openapi.TYPE_STRING,
)


@method_decorator(name="retrieve", decorator=swagger_auto_schema(
    manual_parameters=[CHANNEL_QUERY_PARAM, UTTER_NAME_PATH_PARAM],
    operation_description="Haal een utter op"
))
class UtterResponseViewSet(ModelViewSet):
    """
    Retrieve an Utter
    """

    queryset = UtterResponse.objects.all()
    serializer_class = UtterResponseSerializer
    lookup_field = "utter_name"

    def get_object(self):
        channel = self.request.query_params.get("channel")
        utter = self.kwargs[self.lookup_field]
        qs = self.queryset.filter(utter=utter)

        # If a specific response exists for the channel, return it
        if channel:
            channel_filtered = qs.filter(channel__name=channel)
            if channel_filtered:
                return channel_filtered.first()

        # Fallback: return response without specified channel
        obj = get_object_or_404(qs, utter=utter, channel__name=None)
        return obj
