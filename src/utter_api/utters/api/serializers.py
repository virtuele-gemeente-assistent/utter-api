from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers


class UtterResponseSerializer(serializers.Serializer):
    text = serializers.CharField(help_text=_("The text to return in the response"))
    buttons = serializers.ListField(
        help_text=_("Buttons that will be shown in the response")
    )
    image = serializers.CharField(
        required=False, help_text=_("Optional image to be returned in the response")
    )
    elements = serializers.ListField(help_text=_(
        "CURRENTLY NOT USED"
    ))
    attachments = serializers.ListField(help_text=_(
        "CURRENTLY NOT USED"
    ))

    def to_representation(self, instance):
        slots = {}
        arguments = {}
        # intent_ranking = []
        # product = None

        # channel = request.query_params.get("channel")

        # utterresponse = get_object_or_404(
        #     UtterResponse, utter=utter, channel__name=channel
        # )

        # # Determine if any arguments were given with the intent
        # try:
        #     arguments = serializer.validated_data["arguments"]
        # except Exception as e:
        #     logger.error("Error parsing arguments")
        #     logger.exception(e)

        # # Make use of the filled in (or empty) slots sent by Rasa
        # try:
        #     slots = serializer.validated_data["tracker"]["slots"]
        #     product = slots.get("product", None)
        #     gemeente = slots.get("municipality", None)
        # except Exception as e:
        #     logger.error("Error parsing slots")
        #     logger.exception(e)
        # response_data = instance.data([], arguments)
        # serializer = UtterResponseSerializer(data=response_data)
        # serializer.is_valid()
        return instance.data([], arguments)
