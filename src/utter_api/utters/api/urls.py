from django.conf.urls import url
from django.urls import path

from ..apps import AppConfig
from .api import schema_view
from .views import UtterResponseViewSet

app_name = AppConfig.__name__


urlpatterns = [
    url(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    url(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    url(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
    path(
        "utters/<slug:utter_name>/", UtterResponseViewSet.as_view({"get": "retrieve"}), name="utters"
    ),
]
