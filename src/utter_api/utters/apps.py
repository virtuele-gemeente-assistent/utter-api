from django.apps import AppConfig


class UttersConfig(AppConfig):
    name = "utters"
