
import os

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import yaml

from ...models import Channel, UtterResponse, Hyperlink


class Command(BaseCommand):
    args = 'domain.yml'
    help = "Load domain.yml file to extract and import utters"

    def add_arguments(self, parser):
        parser.add_argument('--filename', type=str)
        parser.add_argument('--stream', type=bytes)
        parser.add_argument('--override', type=bool)

    @transaction.atomic
    def parse_templates(self, responses, override=False):
        municipality = settings.MUNICIPALITY_NAME.lower()

        if override:
            UtterResponse.objects.all().delete()

        for key, value in responses.items():
            utter = key
            utters = value[municipality]
            for utterance in utters:
                if not 'text' in utterance and not 'default' in utterance:
                    continue

                # Filter instead of get_or_create, because duplicates might exist
                results = UtterResponse.objects.filter(utter=utter)
                if not results.exists():
                    response = utterance['text']
                    response_type = "ask_affirmation" if utter == "utter_ask_affirmation" else ""
                    obj = UtterResponse.objects.create(
                        utter=utter, response=response, type=response_type
                    )
                    if 'image' in utterance:
                        obj.image = utterance['image']
                    if 'channel' in utterance:
                        channel, created = Channel.objects.get_or_create(name=utterance['channel'])
                        obj.channel = channel
                    obj.save()

    @transaction.atomic
    def parse_hyperlinks(self, responses, override=False):
        municipality = settings.MUNICIPALITY_NAME.lower()

        if override:
            Hyperlink.objects.all().delete()

        for hyperlink_name, hyperlinks in responses.items():
            if municipality in hyperlinks:
                hyperlink = hyperlinks[municipality]
                Hyperlink.objects.create(name=hyperlink_name, label=hyperlink["label"], url=hyperlink["url"])

    def handle(self, *args, **options):
        filename = options.get('filename')
        stream = options.get('stream')
        override = options.get('override')

        if filename and stream or (not filename and not stream):
            raise CommandError("Supply either --filename or --stream")

        if filename:
            print("Parsing {} file".format(filename))
            with open(filename, 'r') as stream:
                data = yaml.safe_load(stream)
                responses = data.get("responses") or data.get("templates")
                self.parse_templates(responses)
                self.parse_hyperlinks(data.get("hyperlinks", {}), override=override)
        elif stream:
            data = yaml.safe_load(stream)
            responses = data.get("responses") or data.get("templates")
            self.parse_templates(responses, override=override)
            self.parse_hyperlinks(data.get("hyperlinks", {}), override=override)
