from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from django.utils.http import urlencode
from .factories import UtterResponseFactory, ChannelFactory, HyperlinkFactory

class UtterAPITests(APITestCase):

    def setUp(self):
        self.utter1 = UtterResponseFactory.create(utter="utter_ask", response="que?")
        self.channel = ChannelFactory.create(name="voice")
        self.utter2 = UtterResponseFactory.create(utter="utter_ask", channel=self.channel, response="huh?")
        self.utter3 = UtterResponseFactory.create(utter="utter_greet", response="hoi")

    def test_get_utter_default_channel(self):
        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_ask"
        })
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "que?")

    def test_get_utter_specific_channel(self):
        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_ask"
        })
        response = self.client.get(f"{url}?{urlencode({'channel': 'voice'})}")

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "huh?")

    def test_get_utter_404(self):
        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_blegh"
        })
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_utter_specific_channel_default_fallback(self):
        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_ask"
        })
        response = self.client.get(f"{url}?{urlencode({'channel': 'different'})}")

        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data["text"], "que?")

    def test_get_utter_format_hyperlink(self):
        utter = UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)"
        )
        hyperlink = HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort"
        )

        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_paspoort_landingpage"
        })
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "[Ga naar de pagina paspoort](https://dongen.nl/paspoort)")

    def test_get_utter_format_hyperlink_does_not_exist(self):
        utter = UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="[<paspoort_url.label>](<paspoort_url.url>)"
        )

        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_paspoort_landingpage"
        })
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "[<paspoort_url.label>](<paspoort_url.url>)")

    def test_get_utter_format_multiple_hyperlinks(self):
        utter = UtterResponseFactory.create(
            utter="utter_paspoort_landingpage",
            response="Zie: [<paspoort_url.label>](<paspoort_url.url>) of [<id_kaart_url.label>](<id_kaart_url.url>)"
        )
        hyperlink1 = HyperlinkFactory.create(
            name="paspoort_url",
            label="Ga naar de pagina paspoort",
            url="https://dongen.nl/paspoort"
        )
        hyperlink2 = HyperlinkFactory.create(
            name="id_kaart_url",
            label="Ga naar de pagina id-kaart",
            url="https://dongen.nl/id-kaart"
        )

        url = reverse("utter_api:utters", kwargs={
            "utter_name": "utter_paspoort_landingpage"
        })
        response = self.client.get(url)

        data = response.data

        self.assertEqual(data["text"], "Zie: [Ga naar de pagina paspoort](https://dongen.nl/paspoort) of [Ga naar de pagina id-kaart](https://dongen.nl/id-kaart)")
