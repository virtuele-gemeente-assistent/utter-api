from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from django.utils.http import urlencode
from .factories import UtterResponseFactory, ChannelFactory

class UtterAPISchemaTests(APITestCase):

    def test_homepage_redirect(self):
        url = reverse("home")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, reverse("utter_api:schema-redoc"))

    def test_get_schema_redoc(self):
        url = reverse("utter_api:schema-redoc")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_schema_swagger_ui(self):
        url = reverse("utter_api:schema-swagger-ui")
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

