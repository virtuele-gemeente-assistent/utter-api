import requests_mock
import yaml

from django.conf import settings
from django.urls import reverse
from django_webtest import WebTest

from .factories import UserFactory, UtterResponseFactory
from ..models import UtterResponse, Hyperlink


class DomainImportTests(WebTest):

    def setUp(self):
        self.user = UserFactory.create(is_superuser=True, is_staff=True)

    def test_import_domain(self):
        data = '''%YAML 1.1\n---\nresponses:\n\n  utter_greet:\n    dongen:\n    - text: hallo\n'''
        # UtterResponseFactory.create(
        #     utter="utter_greet", response="hoooooooi \U0001F984", gemeente=None
        # )
        # UtterResponseFactory.create(
        #     utter="utter_ask", response="que?", gemeente=None
        # )

        with requests_mock.Mocker() as m:
            m.get(settings.DOMAIN_URL, text=data)
            response = self.app.get(reverse('admin:import_domain'), user=self.user)

        utter = UtterResponse.objects.get()

        self.assertEqual(utter.utter, "utter_greet")
        self.assertEqual(utter.response, "hallo")

    def test_import_domain_override(self):
        data = '''%YAML 1.1\n---\nresponses:\n\n  utter_greet:\n    dongen:\n    - text: hallo\n'''
        UtterResponseFactory.create(
            utter="utter_greet", response="hoooooooi \U0001F984"
        )

        with requests_mock.Mocker() as m:
            m.get(settings.DOMAIN_URL, text=data)
            response = self.app.get(reverse('admin:import_domain'), user=self.user)

        utter = UtterResponse.objects.get()

        self.assertEqual(utter.utter, "utter_greet")
        self.assertEqual(utter.response, "hallo")

    def test_import_hyperlinks_override(self):
        data = '''%YAML 1.1\n---\nresponses:\n\n  utter_greet:\n    dongen:\n    - text: hallo\nhyperlinks:\n\n  landingpage_paspoort_url:\n    dongen:\n      label: testlabel\n      url: https://test.nl'''

        with requests_mock.Mocker() as m:
            m.get(settings.DOMAIN_URL, text=data)
            response = self.app.get(reverse('admin:import_domain'), user=self.user)

        hyperlink = Hyperlink.objects.get()

        self.assertEqual(hyperlink.label, "testlabel")
        self.assertEqual(hyperlink.url, "https://test.nl")

    def test_import_domain_login_required(self):
        data = '''%YAML 1.1\n---\nactions:\n- utter_greet\n\nresponses:\n  utter_greet:\n  - text: hallo\n    default: default'''
        UtterResponseFactory.create(
            utter="utter_greet", response="hoooooooi \U0001F984"
        )

        with requests_mock.Mocker() as m:
            m.get(settings.DOMAIN_URL, text=data)
            response = self.app.get(reverse('admin:import_domain'))

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)


    def test_import_domain_staff_required(self):
        data = '''%YAML 1.1\n---\nactions:\n- utter_greet\n\nresponses:\n  utter_greet:\n  - text: hallo\n    default: default'''
        UtterResponseFactory.create(
            utter="utter_greet", response="hoooooooi \U0001F984"
        )

        with requests_mock.Mocker() as m:
            m.get(settings.DOMAIN_URL, text=data)
            response = self.app.get(reverse('admin:import_domain'), user=UserFactory.create())

        self.assertEqual(response.status_code, 302)
        self.assertIn(reverse("admin:login"), response.url)


