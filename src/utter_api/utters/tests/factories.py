from ..models import Channel, UtterResponse, Hyperlink
from factory.django import DjangoModelFactory
import factory

from utter_api.accounts.models import User


class UserFactory(DjangoModelFactory):

    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'test{}'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')


class ChannelFactory(DjangoModelFactory):

    class Meta:
        model = Channel


class UtterResponseFactory(DjangoModelFactory):

    class Meta:
        model = UtterResponse


class HyperlinkFactory(DjangoModelFactory):

    class Meta:
        model = Hyperlink
