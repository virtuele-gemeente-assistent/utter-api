import logging

from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.management import call_command
from django.http import HttpResponseRedirect
from django.urls import path, reverse
from django.utils.translation import ugettext_lazy as _

import requests
import yaml

from .models import Channel, Hyperlink, UtterResponse

logger = logging.getLogger(__name__)


class ChannelAdmin(admin.ModelAdmin):
    pass


admin.site.register(Channel, ChannelAdmin)


@staff_member_required
def import_domain(request):
    try:
        response = requests.get(settings.DOMAIN_URL)
        if response.status_code == 200:
            logger.info("Importing domain from gitlab")
            call_command('import_yaml', stream=response.content, override=True)
            messages.add_message(request, messages.SUCCESS, _("Domain was successfully imported"))
    except Exception as e:
        logger.exception(f"Failed to import domain from gitlab: {e}")
        messages.add_message(request, messages.ERROR, _("Something went wrong while importing domain"))
    return HttpResponseRedirect(reverse("admin:utters_utterresponse_changelist"))


class UtterResponseAdmin(admin.ModelAdmin):
    list_display = ("utter", "response", "channel", "created_on")
    list_filter = ("channel",)
    search_fields = (
        "utter",
        "response",
    )
    # inlines = [ButtonInline]
    change_list_template = "admin/utterresponse_change_list.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import_domain/', import_domain, name="import_domain")
        ]
        return my_urls + urls
admin.site.register(UtterResponse, UtterResponseAdmin)


class HyperlinkAdmin(admin.ModelAdmin):
    list_display = ("name", "label", "url",)
    search_fields = ("name", "label", "url",)
admin.site.register(Hyperlink, HyperlinkAdmin)
