from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = "utter_api.accounts"
