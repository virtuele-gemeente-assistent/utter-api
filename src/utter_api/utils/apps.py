from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "utter_api.utils"

    def ready(self):
        from . import checks  # noqa
