import sentry_sdk

from .base import *

#
# Standard Django settings.
#

DEBUG = False

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

ALLOWED_HOSTS = ["gem-staging.maykinmedia.nl"]

# Redis cache backend
# NOTE: If you do not use a cache backend, do not use a session backend or
# cached template loaders that rely on a backend.
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",  # NOTE: watch out for multiple projects using the same cache!
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "IGNORE_EXCEPTIONS": True,
        },
    }
}

# Caching sessions.
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# Caching templates.
TEMPLATES[0]["OPTIONS"]["loaders"] = [
    ("django.template.loaders.cached.Loader", RAW_TEMPLATE_LOADERS),
]

LOGGING["loggers"].update(
    {
        "django": {"handlers": ["django"], "level": "INFO", "propagate": True,},
        "django.security.DisallowedHost": {
            "handlers": ["django"],
            "level": "CRITICAL",
            "propagate": False,
        },
    }
)

#
# Custom settings
#

# Show active environment in admin.
ENVIRONMENT = "staging"

# We will assume we're running under https
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True

# SAMEORIGIN by default. Enable the following if no iframes are used
# X_FRAME_OPTIONS = 'DENY'
# Only set this when we're behind Nginx as configured in our example-deployment
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
SECURE_CONTENT_TYPE_NOSNIFF = True  # Sets X-Content-Type-Options: nosniff
SECURE_BROWSER_XSS_FILTER = True  # Sets X-XSS-Protection: 1; mode=block


#
# Library settings
#

ELASTIC_APM["SERVICE_NAME"] += " " + ENVIRONMENT

# Sentry SDK
SENTRY_CONFIG = {
    "dsn": "https://a22910ff0ae7441eb5f77415a6229529@sentry.maykinmedia.nl/165",
    "public_dsn": "https://a22910ff0ae7441eb5f77415a6229529@sentry.maykinmedia.nl/165",
    "release": os.getenv("VERSION_TAG", "VERSION_TAG not set"),
}

sentry_sdk.init(
    dsn=SENTRY_CONFIG["dsn"],
    release=SENTRY_CONFIG["release"],
    integrations=SENTRY_SDK_INTEGRATIONS,
    send_default_pii=True,
)

# APM
# MIDDLEWARE = ["elasticapm.contrib.django.middleware.TracingMiddleware"] + MIDDLEWARE
# INSTALLED_APPS = INSTALLED_APPS + [
#     "elasticapm.contrib.django",
# ]

# URL to retrieve domain.yaml from (test branch)
DOMAIN_URL = 'https://gitlab.com/virtuele-gemeente-assistent/gem/-/raw/test/rasa/local.yml'
