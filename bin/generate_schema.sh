#!/bin/bash

# Run this script from the root of the repository

set -e

if [[ -z "$VIRTUAL_ENV" ]]; then
    echo "You need to activate your virtual env before running this script"
    exit 1
fi

echo "Generating Swagger schema"
src/manage.py generate_swagger \
    ./src/swagger2.0.json \
    --overwrite \
    --format=json \
    --mock-request \
    --url https://utter-api.com/api/v1 \
    --settings utter_api.conf.dev

echo "Converting Swagger to OpenAPI 3.0..."
# npm run convert
swagger2openapi src/swagger2.0.json -o src/openapi.yaml
# patch_content_types

echo "Generating resources document"
src/manage.py generate_swagger \
    ./src/resources.md \
    --overwrite \
    --mock-request \
    --url https://utter-api.com/api/v1 \
    --settings utter_api.conf.dev
