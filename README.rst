==================
utter_api
==================

:Version: 0.1.0
:Source: https://bitbucket.org/maykinmedia/utter_api
:Keywords: ``<keywords>``
:PythonVersion: 3.7

|build-status| |requirements|

An Utter API implementation for municipality specific utters to a chatbot

Developed by `Maykin Media B.V.`_ for Gemeente Tilburg


Introduction
============

This API allows the country wide Chatbot to retrieve utters specific to a
municipality via NLX

Documentation
=============

See ``INSTALL.rst`` for installation instructions, available settings and
commands.


References
==========

* `Issues <https://taiga.maykinmedia.nl/project/utter_api>`_
* `Code <https://bitbucket.org/maykinmedia/utter_api>`_


.. |build-status| image:: http://jenkins.maykin.nl/buildStatus/icon?job=bitbucket/utter_api/master
    :alt: Build status
    :target: http://jenkins.maykin.nl/job/utter_api

.. |requirements| image:: https://requires.io/bitbucket/maykinmedia/utter_api/requirements.svg?branch=master
     :target: https://requires.io/bitbucket/maykinmedia/utter_api/requirements/?branch=master
     :alt: Requirements status


.. _Maykin Media B.V.: https://www.maykinmedia.nl
